import operator
from Graph_Interactions import Interactome, Proteome, DomainGraph
import pickle
import urllib.request
import json
import os.path
import time

#prot_missed_list=['CALM_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'PO121_HUMAN', 'AMY1_HUMAN', 'AMY1_HUMAN', 'AMYS_HUMAN', 'PO121_HUMAN', 'HSP71_HUMAN', 'PO121_HUMAN', 'CALM_HUMAN', 'HSP71_HUMAN', 'PO121_HUMAN', 'UBIQ_HUMAN', 'YWHAD', 'HSP71_HUMAN', 'HSP71_HUMAN', 'Q9PH0', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'HNPRD', 'PO121_HUMAN', 'Q9PH0', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'UBIQ_HUMAN', 'YWHAD', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'PO121_HUMAN', 'CGHB_HUMAN', 'MGC42630', 'HSP71_HUMAN', 'TBA2_HUMAN', 'CALM_HUMAN', 'TPPC2_HUMAN', 'UBIQ_HUMAN', 'HNPRD', 'ES1_HUMAN', 'UBIQ_HUMAN', 'HSP71_HUMAN', 'UBIQ_HUMAN', 'G45IP3', 'G45IP3', 'G45IP3', 'G45IP3', 'G45IP3', 'G45IP3', 'Q9PH0', 'UBIQ_HUMAN', 'PO121_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'PO121_HUMAN', 'PO121_HUMAN', 'UBIQ_HUMAN', 'Q9PH0', 'UBIQ_HUMAN', 'HNPRD', 'HSP71_HUMAN', 'HPIK1', 'HSP71_HUMAN', 'HSP71_HUMAN', 'HSP71_HUMAN', 'HSP71_HUMAN', 'UBIQ_HUMAN', 'LAC_HUMAN', 'IAA1049', 'LAC_HUMAN', 'PO121_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'CALM_HUMAN', 'CALM_HUMAN', 'MGC42630', 'LAC_HUMAN', 'LAC_HUMAN', 'LAC_HUMAN', 'PO121_HUMAN', 'LAC_HUMAN', 'CALM_HUMAN', 'RPO1D_HUMAN', 'TPPC2_HUMAN', 'MGC42630', 'MGC39821', 'RPS6KB3', 'UBIQ_HUMAN', 'AMY1_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'TBA2_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'PFND6', 'HSP71_HUMAN', 'PO121_HUMAN', 'PO121_HUMAN', 'PO121_HUMAN', 'PO121_HUMAN', 'PO121_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'CALM_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'CSH_HUMAN', 'UBIQ_HUMAN', 'PO121_HUMAN', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'Q9PH0', 'UBIQ_HUMAN', 'YWHAD', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'RPO1D_HUMAN', 'UBIQ_HUMAN', 'SEDLP', 'SAA_HUMAN', 'UBIQ_HUMAN', 'SIP1_HUMAN', 'SIP1_HUMAN', 'SIP1_HUMAN', 'SIP1_HUMAN', 'TNRC17', 'UBIQ_HUMAN', 'CSH_HUMAN', 'UBIQ_HUMAN', 'CSH_HUMAN', 'CSH_HUMAN', 'UBIQ_HUMAN', 'SSB4_HUMAN', 'UBIQ_HUMAN', 'TBB2_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'UBIQ_HUMAN', 'CSH_HUMAN', 'TPPC2_HUMAN']
#num missed prot: 191

i_obj=Interactome('Human_HighQuality.txt')
g_obj=DomainGraph()
with open("prot_domain_dict.json", "r") as jsonFile:
    prot_dom_dict = json.load(jsonFile)
with open("generate_cooccurrence_graph_object_list.json", "r") as jsonFile:
        dom_dom_list = json.load(jsonFile)

# i_obj.ls_proteins()
# i_obj.ls_domain()
# i_obj.ls_domains_n(10)
# i_obj.nbDomainsByProteinDistribution()
# i_obj.nbProteinsByDomainDistribution()
# i_obj.co_occurrence("S8_pro-domain", "Bromodomain", prot_dom_dict)
# i_obj.weighted_cooccurrence_graph()

#Answer 6.2.10: the number of least related domains is bigger than ten
#(there is doms with equal numbers of relation)

# g_obj.generate_cooccurrence_graph()
# g_obj.read_interaction_file_dict(dom_dom_list)
# g_obj.density_d()
# g_obj.ten_most_related_dom()
# g_obj.ten_least_related_dom()
# g_obj.most_dom_present_compar_most_related()
# g_obj.domains_cooccurring_themselves()
# g_obj.generate_cooccurrence_graph_np(10)
# g_obj.graph_n_density(64)
# g_obj.generate_cooccurrence_graph_n(10)
# g_obj.creat_file_interaction_dom_dom(m.weighted_cooccurrence_graph())
# g_obj.graph_threshold(10)
# g_obj.proba_cooccurrence("S8_pro-domain", "Bromodomain")

# 6.3.2 teste de chi2:Comparison of two percentages
# H0(null hypothesis)= the two frequences are equal,
# H1 (alternative hypothesis)= the two frequencies are different

# g_obj.different_frequency()
# g_obj.proba_cooccurrence_three_doms("S8_pro-domain", "Bromodomain")
# g_obj.proba_cooccurrence_n_doms("S8_pro-domain", "Bromodomain", 5)
# g_obj.different_frequency_n_doms(5)











