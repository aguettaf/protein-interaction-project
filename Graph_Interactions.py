# -*- coding: utf-8 -*-
"""
@author: Abdelkader Guettaf

"""
import csv

import numpy as np
import urllib.request
import matplotlib.pyplot as plt
import json
import operator
import time

from matplotlib import pyplot


class Interactome():
    """
    Graph_interactions: is a class which allows reading and manipulation of
    a graph of interactions between proteins (file)
    """

    def __init__(self, file):
        """
        This constructor creat an object with two attributs (List, an itirable file)

        :param file:The file of proteins interactions (text with tabs)
        """

        with  open(file, 'r') as file_1:
            # file list instance
            file_list = file_1.readlines()[1:]
        self.file_list = file_list

        #file itirable instance
        with open(file, 'r') as file_2:
            next(file_2)
        self.file_itirable=file_2

        #list of proteins instance
        with open(file, 'r') as file_3:
            prot_list=[]
            for line in file_3:
                line_list = line.split()
                prot_list.extend(line_list)
        self.prot_list = list(set(prot_list))

        #Dictionnary interaction file instance
        self.inter_dict=self.read_interaction_file_dict()

    def __str__(self):
        """
        this function print the attributs of the object (list, the itirable object of file)

        :return: str: list , itirable object of the file
        """


        return 'The list:' +'\n' + str(self.file_list) +'\n'+ \
               'The dictionary :' +'\n'+ str(self.inter_dict)


    def read_interaction_file_dict(self):
        """
        This function creat a dictionary where the key is the protein and the value
        is the related proteins

        :param self: the object of the file of protein interactions
        :return: dict {protein:[list of related proteins]}
        """


        file_list=self.file_list
        inter_dict = {}
        for line in file_list:
            line_list = line.split()
            if line_list[0] not in inter_dict:
                inter_dict[line_list[0]] = []
                if line_list[1] not in inter_dict[line_list[0]]:
                    inter_dict[line_list[0]].append(line_list[1])
            else:
                if line_list[1] not in inter_dict[line_list[0]]:
                    inter_dict[line_list[0]].append(line_list[1])
            if line_list[1] not in inter_dict:
                inter_dict[line_list[1]] = []
                if line_list[0] not in inter_dict[line_list[1]]:
                    inter_dict[line_list[1]].append(line_list[0])
            else:
                if line_list[0] not in inter_dict[line_list[1]]:
                    inter_dict[line_list[1]].append(line_list[0])

        return inter_dict



    def read_interaction_file_list(self):
        """
        this function return a list of tuples where each tuple contains
        an interaction between two proteins

        :return: list of tuples
        """

        inter_list = []
        file_list = self.file_list
        for line_list in file_list:
            element_list = line_list.split()
            prot_1 = element_list[0]
            prot_2 = element_list[1]
            inter_list.append((prot_1, prot_2))
        return inter_list


    def read_interaction_file_mat(self):
        """
        this function creat an adjacency matrix where in each interaction between
        two proteins it put 1 in the intersection of column and line in the matrix

        :return: array adjacency matrix of interactions between proteins
        """

        column_list = []
        row_list = []
        inter_list = [] # inter_list contains all the file in a list to use it later in the comparison
        file_list=self.file_list
        for line in file_list:
            line_list = line.split()
            inter_list.append(line_list)
            column_list.append(line_list[0])
            row_list.append(line_list[1])
        row_list = list(set(row_list)) # set: to delete the dubbled elements in the colums and the lines
        column_list = list(set(column_list))
        # creation of matrix with the dimention of file's line
        matrix = np.zeros((len(row_list), len(column_list)))
        for column_element in column_list:
            for row_element in row_list:
                for elements_tuple in inter_list:
                    if [column_element, row_element] == elements_tuple:
                        matrix[row_list.index(row_element)][column_list.index(column_element)] = 1

        return matrix

    def read_interaction_file(self):
        """
        this function return a tuple where the first element is the dictionary of file of interactions,
        the second element is a list of the interaction with the first protein of this dictionary

        :return: tuple ({dictionary, list})
        """

        inter_dict = self.inter_dict
        inter_list = self.read_interaction_file_list()
        inter_tuple=(inter_dict, inter_list)
        return inter_tuple

    def count_vertices(self):
        """
        this function counts the number of vertices of a graph

        :return (str) the number of vertices in this graph
        """
        inter_dict = self.inter_dict
        numb_vert = 0
        for i in inter_dict.keys():
            numb_vert += 1
        return numb_vert

    def count_edges(self):
        """
        this function counts the number of edges in a protein interactions graph

        :return (str) the number of edges in this graph
        """
        num_edge = 0
        inter_dict = self.inter_dict
        for edges_list in inter_dict.values():
            num_edge += len(edges_list)
        return num_edge


    def clean_interactome(self):
        """
        this function creat a new file without redundant interactions, and without
        the homo-dimers proteins

        :return file : new file
        """

        file_list = self.file_list
        element1_list = []
        element2_list = []
        inter_list = []
        for line in file_list:
            prot_line_list = line.split()
            inter_list.append(tuple(prot_line_list))
            element1_list.append(prot_line_list[0])
            element2_list.append(prot_line_list[1])
        element1_list = list(set(element1_list))
        element2_list = list(set(element2_list))
        inter_list=list(set(inter_list)) #delete redundant interactions

        # delete of homo-dimers:(homodemers: interaction between two same proteins)
        for prot_inter in inter_list:
            if prot_inter[0] == prot_inter[1]:
                inter_list.remove(prot_inter)

        #the creat of the new file
        with open(r'new_inter_file', 'a+', encoding='utf8') as new_file:
            num_lines = 0
            for elem_1 in element1_list:
                for elem_2 in element2_list:
                    for elem_tuple in inter_list:
                        if [elem_1, elem_2] == elem_tuple:
                            num_lines += 1
                            new_file.write(f'{elem_1}	{elem_2}\n')

        # to add the first line of number of reactions to the file
        n_file = open(r'new_inter_file', 'r', encoding='utf8')
        new_file_list = n_file.readlines()
        n_file.close()
        new_file_list.insert(0, f'{num_lines}\n')#in the index zero insert the number of lines
        new_file_final = open(r'new_inter_file', 'w', encoding='utf8')
        new_file_final.writelines(new_file_list)
        new_file_final.close()
        return 'the file has been created'


    def get_degree(self, prot):
        """
        this function return the degree (number of relations) of a selected protein

        :arg prot the name of protein wanted
        :return int : the degree of the summet
        """

        inter_dict = self.inter_dict
        return len(inter_dict[prot])


    def proteins_degrees_dict(self):
        """
        this function return a dictionary {protein: his degree}
        the degree (number of relations) of every protein

        :return dict : {protein:degre_of_this_protein}
        """

        inter_dict = self.inter_dict
        prot_degre_dict={}
        for prot, neighbors_prot in inter_dict.items():
            prot_degre_dict[prot]=len(neighbors_prot)
        return prot_degre_dict

    def get_max_degree(self):
        """
        this function return the protein that has the maximum number of degrees

        :return (tuple) the protein with max degree, and his number of degree
        """

        p_degre_dict = self.proteins_degrees_dict()
        max_deg=0
        max_prot_deg_tuple=()
        for prot, deg in p_degre_dict.items():
            if deg >max_deg:
                max_deg=deg
                max_prot_deg_tuple=(prot, deg)

        return f'the maximum number of degrees: protein: {max_prot_deg_tuple[0]} number of degree: {max_prot_deg_tuple[1]} '

    def get_ave_degree(self):
        """
        this function return the average of degrees of all the proteins of this file

        :return int :the average of all protein's degrees
        """

        prot_deg_dict = self.proteins_degrees_dict()
        sum_value=sum(prot_deg_dict.values())
        num_protein=len(prot_deg_dict.keys())

        average = sum_value / num_protein
        return (f'The average of degrees: {average}')

    def count_degree(self, deg):
        """
        this function return the number of proteins that have the degree entered by user

        :arg deg the degree wanted
        :return str the name of the protein
        """

        prot_deg_dict = self.proteins_degrees_dict()
        num_protein = 0
        for dict_deg in prot_deg_dict.values():
            if dict_deg == deg:
                num_protein += 1

        return (f'The number of proteins with the degre of {deg} : {num_protein}')

    def histogram_degree(self, dmin, dmax):
        """
        this function return an histogram with stars, of the number of degrees of each
        protein that exist in the field of degrees entered by user (between dmin, dmax)

        :arg dmin The lower limit of the field wanted
        :arg dmax The higher limit of the field wanted
        :return str histogram with stars
        """

        prot_deg_dict = self.proteins_degrees_dict()
        num_line = 0
        hist_str = 'The histogram degree: \n'
        for deg in prot_deg_dict.values():
            if deg >= dmin and deg <= dmax:
                num_line += 1
                hist_str += "\n {:<6}".format(num_line)
                for hist_line in range(0, deg):
                    hist_str += '*'
        return hist_str

    def component_connex(self):
        """
        This function return a list of lists that represent the connected component
        of a protein graph of interaction

        :return:list the connected components
        """

        inter_dict = self.inter_dict
        prot_list = list(inter_dict.keys())
        all_ccs_list = []
        while len(prot_list) != 0:
            cc_list = [prot_list[0]]
            prot_list.remove(prot_list[0])
            for key in cc_list:
                for value in inter_dict[key]:
                    if value not in cc_list:
                        cc_list.append(value)
                        prot_list.remove(value)
            all_ccs_list.append(cc_list)
        return all_ccs_list

    def countCC(self):
        """
        This function return a string, where in each line a connected component
        with the number of its summit

        :return: String the connected components, their number, and the number
        of summit of each one of them
        """

        cc = self.component_connex()
        num_cc_str = ''
        for num in cc:
            num_cc_str += f'{len(num)} {num}\n'
        num_cc_str += f'\nThe total number of CC is: {len(cc)}'
        return num_cc_str

    def writeCC(self):
        """
        this function creat a new file contains all the connected components,
        of a protein graph of interaction

        :return: a new file contains all the connected components, their total
        number, and the number of summit of each one of them
        """

        with open('CC_file', 'a+') as cc_file:
            cc_file.write(self.countCC())


    def extractCC(self, prot):
        """
        This function extract the the connected component of a selected protein

        :param prot: the protein introduced
        :return: list: the connected component
        """

        com_conx_list = self.component_connex()
        for comp_conx in com_conx_list:
            if prot in comp_conx:
                return comp_conx

    def computeCC(self):
        """
        This function returns a list lcc where each lcc [i] element corresponds
        to the related component number of the protein at position i in the list
        of proteins in the graph (which is an attribute of our class).

        :return: list contains the compenent connect classified by the position (index)
        of their proteins in protein list
        """

        prot_list = self.prot_list
        cc_list = self.component_connex()
        lcc=[]
        for prot in prot_list:
            i=prot_list.index(prot)# variable i used in the article (index of prot)
            for comp_conx in cc_list:
                if prot in comp_conx:
                    prot_cc= comp_conx
                    lcc_i=cc_list.index(prot_cc) #the position of cc
                    lcc.append(lcc_i)

        return lcc

    def density(self):
        """
        This function calculate the density of a protein graph of interaction

        :return: float: the density of the protein graph of interactions
        """

        number_edges = self.count_edges()
        prot_list = len(self.prot_list)
        # calculate the density
        possible_edges = int(int(prot_list) * (int(prot_list) - 1))
        density_graph = number_edges / possible_edges

        return density_graph

    def clustering(self, prot):
        """
        This function calculate the local clustering coefficient of a
        node/summit 'prot'

        :param prot:the summit selected
        :return:float: the local clustering coefficient of this summit
        """

        inter_dict = self.inter_dict
        num_triangle = 0
        pair_neighbour = (len(inter_dict[prot]) * (len(inter_dict[prot]) - 1)) / 2
        for value1 in inter_dict[prot]:
            for value2 in inter_dict[value1]:
                if value1 in inter_dict[value2]: #value1 has valus2 in liste and value2 has value 1 in list
                    num_triangle += 1
        if pair_neighbour != 0:
            result= num_triangle / pair_neighbour
        else:
            result ='0, "No neighbour for this protein"'
        return result

#############################################################
########################///CH 6\\\###########################
#############################################################

    def ls_proteins(self):
        """
        This function return the list non-redundant of all the proteins found
        in the protein interactions graph protein.

        :return:list: list of all proteins
        """
        prot_list=self.prot_list #an attribut in Interactom class created in the first chapter

        return prot_list

    def prot_domain_dict(self):
        """
        This function return a dictionnary where the keys are proteins
        and the values are the list of their domains
        nb: I add this function to accelerate the use of information

        :return:dict: {protein: [list of domain of prot]}
        """

        domains_pro_dict={}
        obj = Proteome('uniprot-list.txt') # creat an object to use the methods of proteome class

        for protein in self.ls_proteins():
            domains_pro_dict[protein] = obj.getProteinDomains(protein)
        # {domain:num domain}

        # serialisation of domains list
        with open("prot_domain_dict.json", "w") as jsonFile:
            json.dump(domains_pro_dict, jsonFile)

    def ls_domain(self):
        """
        This function return the list non-redundant of all the domains found in
        the proteins that make up the protein-protein interactions graph.

        :return:list: list of all domains of the proteins interactions graph
        """

        #deserialization {prot,domains}
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        domains_list=[]
        for doms in prot_dom_dict.values():
            domains_list.extend(doms)
        domains_list = list(set(domains_list))
        return domains_list


    def ls_domains_n(self, n):
        """
        This function returns the non-redundant list of all domains
        that are found at least n times in the proteins that make up
        the protein-protein interaction graph.

        :param n: int - number of times the domain is present in proteins
        :return: list: list of domains repeated n times
        """

        # deserialization {prot,domains}
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        domains_list = [] #list domains with redundancy
        for doms in prot_dom_dict.values():
            domains_list.extend(doms)
        dom_counted_list = dict([(k, domains_list.count(k)) for k in set(domains_list) if
                       domains_list.count(k) >= n])  # list of tuples -> dict
        dom_list = list(dom_counted_list)
        return dom_list


    def nbDomainsByProteinDistribution(self):
        """
        This function returns a dictionary whose keys correspond to
        the number of domains found in proteins, and the value associated
        with each key is the number of proteins with that number of domains.

        :return: dict: {protein_number:domains number}
        """

        # deserialization {prot,domains}
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        n_prot=0
        nprot_ndom_dict={}
        x_dom=[]
        y_prot=[]
        for prot,dom in prot_dom_dict .items():
            n_prot+=1
            nprot_ndom_dict[len(dom)]=n_prot
            x_dom.append(len(dom))
            y_prot.append(n_prot)
        #histogram
        hist_str=''
        for indx in range(0,len(y_prot)):
            hist_str+='\n'
            hist_str += str(x_dom[indx])+ ' '
            for stars in range(0,y_prot[indx]):
                hist_str+='*'
        return f'{nprot_ndom_dict}, \n{hist_str}' #1:8 = the proteins that have 1 domains are 8


    def nbProteinsByDomainDistribution(self):
        """
        This function a dictionary whose keys correspond to the numbers of
        proteins associated with a domain, and the value associated with
        each key is the number of domains present in this number of proteins.

        :return: dict {n_prot: number domains}, histogram
        """

        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        n_prot = 0
        nprot_ndom_dict = {}
        y_dom = []
        x_prot = []
        for prot, dom in prot_dom_dict.items():
            n_prot += 1
            nprot_ndom_dict[n_prot] = len(dom)
            y_dom.append(len(dom))
            x_prot.append(n_prot)
        # histogram
        hist_str = ''
        for indx in range(0, len(x_prot)):
            hist_str += '\n'
            hist_str += str(y_dom[indx]) + ' '
            for stars in range(0, x_prot[indx]):
                hist_str += '*'
        return f'{nprot_ndom_dict}, \n{hist_str}'


    def co_occurrence(self, dom_x, dom_y, prot_dom_dict):
        """
        This function returns the number of co-occurrences of the x and y domains
        in the proteins of our interactome.

        :param dom_x: domain selected
        :param dom_y: domain selected
        :param dict_prot_dom: i added dict_prot_dom as a parameter to accelerate
        the work of function with big files; {protein:[domains]} dictionnary file
        :return: int, number of  of co-occurrences of the x and y domains
        """

        num_co=0
        for doms_list in prot_dom_dict.values():
            if dom_x in doms_list and dom_y in doms_list:
                num_co+=1

        return num_co


    def weighted_cooccurrence_graph (self):
        """
        This function returns a new instance where the vertices are the domains
        with the weight of this interaction (the sum of co-occurence of first domain and the second domain in a protein) .
        There must be an interaction between the vertices x and y only when
        these two domains are co-occurring.

        :return:list of tuples: [(domains 1, domains 2, the weight of this interaction)]
        """

        dom_dom_weight_list = []
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)

        for prot in prot_dom_dict:
            for dom1 in prot_dom_dict[prot]:  # list of domains
                for dom2 in prot_dom_dict[prot]:
                    if dom1 != dom2:  # to do not put d_d
                        dom_dom_weight_list.append((dom1, dom2, self.co_occurrence(dom1, dom2, prot_dom_dict)))
        dom_dom_weight_list=list(set(dom_dom_weight_list))
        # with open("weighted_cooccurrence_graph.json", "w") as jsonFile:
        #     json.dump(dom_dom_weight_list, jsonFile)

        return dom_dom_weight_list

class Proteome ():
    """
    Proteome: is a class which product a dictionary where the keys are proteins and every protein
    has as value another dictionary with three keys : voisins, domains, UniprotID, with lists as values
    new form:
    new_dict={'A':{'domaine':['domaineA1','domaineA2', 'domaineA3', 'domaineA2'], 'voisins':['Prot C', 'Prot B']}}
    """

    def __init__(self, file):
        """
        This constructor creat an object with three attributs (old dictionnary : for the
        dictionnary that come from read_interaction_file_dict of Interactome classe,
        List_prot for liste of proteins and dicto for a dioctionary :
        diction={identifiant UniprotID: protein associated})

        :param file: Uniprot list of identifiants
        """

        r_object=Interactome("Human_HighQuality.txt")
        self.inter_old_dict= r_object.read_interaction_file_dict()

        #list of tuples [(prot,uniprot_id)]
        with open(file, 'r') as uniprot_id_file:
            prot_id_list = []
            next(uniprot_id_file)
            for line in uniprot_id_file:
                line_list = line.split()
                line_tuple = (line_list[0], line_list[1])
                prot_id_list.append(line_tuple)

        self.prot_id_list=prot_id_list

        prot_proteom_list = []
        id_prot_dict={}

        with open(file, 'r') as uniprot_list:
            next(uniprot_list)
            for line2 in uniprot_list:
                line2_list=line2.split()
                prot_proteom_list.append(line2_list[0])
                id_prot_dict[line2_list[1]]= line2_list[0]

        self.prot_proteom_list = prot_proteom_list
        self.id_prot_dict = id_prot_dict #id_prot_dict{id UniprotID: its protein}

    def xlinkUniprot(self):
        """
        This function add two keys to the value (dictionnary) of each protein : voisins: for protein neighberhood
        and UniprotID for the identifiant of protein on uniprot

        :return:dict: the new dictionnary :
        new_dict={'A':{'voisins':['Prot C', 'Prot B'], 'UniprotID':'Identifiant_of_protin'}}
        """

        graphe_dict={}
        inter_old_dict = self.inter_old_dict
        for k_prot,v_prots in inter_old_dict.items():
            id_inter_dict={}
            id_inter_dict['voisins']=v_prots

            for tuple_el in self.prot_id_list:
                if k_prot == tuple_el[0]:
                    id_inter_dict['UniprotID'] = tuple_el[1]
            graphe_dict[k_prot]=id_inter_dict
        return graphe_dict

    def getProteinDomains(self, p):
        """
        This function extract for Uniprot data base the domains of a selected protein.

        :param p:the protein introduced
        :return: list : list of domains of the protein introduced
        """

        # Temporary use of serialization to speed up the function
        # with open("dict_graphe.json", "r") as jsonFile:
        #     dict_prot_dom = json.load(jsonFile)
        # p_id = dict_prot_dom[p]['UniprotID']

        p_id=self.xlinkUniprot()['UniprotID']
        with urllib.request.urlopen(f"https://www.uniprot.org/uniprot/{p_id}.txt") as url:
            web_string = str(url.read())  # url.read type is binary
            w_list = web_string[web_string.find('Pfam'):].split()
            domain_list = []
            for i in w_list[2::4]:
                if w_list[w_list.index(i) - 2] == 'Pfam;':
                    domain_list.append(i[:-1])
            domain_list = list(domain_list)
            return domain_list

    def xlinkDomains(self, p):
        """
        This function,for each protein introduce, it expand the dictionary data structure
        by adding the protein domains.

        :param p: the introduced protein
        :return:dict : new dictionnary with the domains of the protein selected
        new_dict={'A':{'voisins':['Prot C'], 'UniprotID':'Identifiant','domains':['domain A']}}
        """

        id_inter_dict=self.xlinkUniprot()
        domains_list=self.getProteinDomains(p)
        id_inter_dict[p]['domains'] = domains_list
        return id_inter_dict

class DomainGraph ():

    def generate_cooccurrence_graph(self):
        """
        This function returns a list of tuples that represent a graph of interactions
        of domains. There must exist an interaction between the vertices x
        and y only when these two domains are co-occurring in at least one
        protein of the graph.

        :return: list: list of tuples [(dom1,dom2)]
        """

        obj=Interactome('Human_HighQuality.txt')#to use the method cooccurence

        # deserialization {prot,domains}
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        doms_list=obj.ls_domain()
        dom_dom_list=[]
        n_prot=0

        #first method: when we eliminate (dom_1,dom_1) interaction (all domains have same name)
        # for dom1 in doms_list:
        #     n += 1
        #     for dom2 in doms_list:
        #         if dom1 != dom2:
        #             if obj.co_occurrence(dom1,dom2, dict_prot_dom )>= 1:
        #                 lt_dom_dom.append((dom1,dom2))

        #Second method :we delete only the dom used : we can find
        #we can find (dom_1,dom_1) two different domains that have same name,
        for dom1 in doms_list:
            shalowcopy=doms_list[:]
            shalowcopy.remove(dom1)
            n_prot += 1 # just to indicate in which step i am (huge files)
            for dom2 in shalowcopy:
                if obj.co_occurrence(dom1, dom2, prot_dom_dict) >= 1:
                    dom_dom_list.append((dom1, dom2))
            print(n_prot)

        # serialisation of the interactions graph of domains (i used for huge file)
        # with open("generate_cooccurrence_graph_object_list.json", "w") as jsonFile:
        #     json.dump(lt_dom_dom, jsonFile)

        return dom_dom_list

    def __init__(self):
        """
        This constructor creat an object with two attributs (List: domains interactions , list: domain interaction with weight)

        """

        # self.lt_dom_dom = self.generate_cooccurrence_graph()
        obj=Interactome('Human_HighQuality.txt') #to use its function Interactome.weighted_cooccurrence_graph
        self.weighted_cooccurrence_graph=obj.weighted_cooccurrence_graph()


    def read_interaction_file_dict(self, dom_dom_list):
        """
        This function creat a dictionary where the key is a domain and the value
        is the related domains
        nb:i added this function to use it in the next functions

        :return: dict {domain:[list of related domains]}
        """
        # deserialisation in the case of huge files
        # with open("generate_cooccurrence_graph_object_list.json", "r") as jsonFile:
        #     lt_dom_dom = json.load(jsonFile)

        inter_dict = {}
        for i in dom_dom_list:
            if i[0] not in inter_dict :
                inter_dict [i[0]] = []
                if i[1] not in inter_dict [i[0]]:
                    inter_dict [i[0]].append(i[1])
            else:
                if i[1] not in inter_dict [i[0]]:
                    inter_dict [i[0]].append(i[1])
            if i[1] not in inter_dict :
                inter_dict [i[1]] = []
                if i[0] not in inter_dict [i[1]]:
                    inter_dict [i[1]].append(i[0])
            else:
                if i[0] not in inter_dict [i[1]]:
                    inter_dict [i[1]].append(i[0])

        # serialisation of dictionnary of domains interactions
        # with open("read_interaction_file_dict.json", "w") as jsonFile:
        #     json.dump(dicto, jsonFile)
        return inter_dict



    def density_d(self):
        """
        This function calculate the density of a domain graph of interaction

        :return: float: the density of the domain graph of interactions
        """

        with open("read_interaction_file_dict.json", "r") as jsonFile:
            read_interaction_dict = json.load(jsonFile)

        # Count of the edges number in the graph
        number_edges = 0
        for list_dom in read_interaction_dict.values():
            number_edges += len(list_dom)

        # Possible_edges
        obj = Interactome('Human_HighQuality.txt')
        list_dom = len(obj.ls_domain())
        possible_edges = int(int(list_dom) * (int(list_dom) - 1))

        # Calculate the density
        density_graph = number_edges / possible_edges

        return density_graph


    def ten_most_related_dom(self):
        """
        This function returns the 10 domains with the highest number of neighbors

        :return: List: ordered list of tuples [(dom, number of related domains)]
        """

        num_rel_dict={} #dom:num of relations
        most_related_dom_list=[]
        with open("read_interaction_file_dict.json", "r") as jsonFile:
            read_interaction_dict = json.load(jsonFile)

        for dom , related_doms in read_interaction_dict.items():
            num_rel_dict[dom]=len(related_doms)

        for itiration in range(0,10):
            maxi = max(num_rel_dict.items(), key=operator.itemgetter(1))[0]
            #itemgetter(1):classified according to the second element
            #[0] to select the first element of the tuple/dict => to select the most related dom

            most_related_dom_list.append(maxi)
            del num_rel_dict[maxi]
        return list(set(most_related_dom_list))

    def ten_least_related_dom(self):
        """
        This function returns the 10 domains with the lowest number of neighbors

        :return: List: list of tuples [(dom, number of related domains)]
        """
        least_related_dom_list=[]
        numrel_dict={} #dom:num of relations
        least_related_dom_list=[]
        with open("read_interaction_file_dict.json", "r") as jsonFile:
            read_interaction_dict = json.load(jsonFile)
        for dom , related_doms in read_interaction_dict.items():
            numrel_dict[dom]=len(related_doms)
        for itiration in range(0, 10):
            mini = min(numrel_dict.items(), key=operator.itemgetter(1))[0]
            least_related_dom_list.append(mini)
            del numrel_dict[mini]

        return list(set(least_related_dom_list))

#Answer 6.2.10: the number of least related domains is bigger than ten
#(there is doms with equal numbers of relation)

    def most_dom_present_compar_most_related(self):
        """
        This function compare the domains that are present the most times
        in proteins to the domains with the highest number of neighbors

        :return: boolean: True:if two list are same or False
        """

        #1-The domains that are present the most times in proteins of begining
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)

        domains_list = []  # list domains with redundancy
        for doms in prot_dom_dict.values():
            domains_list.extend(doms)

        most_present_dom_list=[]
        num_rel_dict = dict([(k, domains_list.count(k)) for k in set(domains_list) ])  # list of tuples -> dict

        for itiration in range(0, 10):
            maxi = max(num_rel_dict.items(), key=operator.itemgetter(1))[0]
            most_present_dom_list.append(maxi)
            del num_rel_dict[maxi]
        print('most present:',most_present_dom_list)

        #2-Domains with the highest number of neighbors
        most_related_dom_list=self.ten_most_related_dom()
        print('most related:', most_related_dom_list)
        for i in most_present_dom_list:
            if i not in most_related_dom_list:
                return f'{False},(there is some of them but not all)'


    def domains_cooccurring_themselves(self):
        """
        This function returns the number of domains cooccurring with themselves

        :return: int: the number of domains cooccuring with themselves
        """

        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)

        obj=Interactome('Human_HighQuality.txt')
        num_coocurence=0
        for dom in obj.ls_domain():
            if obj.co_occurrence(dom, dom, prot_dom_dict) >1:
                num_coocurence+=1
        return num_coocurence

    def dom_dom_num_occurence_proteins(self):
        """
        This function returns list of [(domain_domain, number of
        cooccurence in the proteins)]
        nb: i added this function to use it in the next functions

        :return: list of tuples:noun of domains dom_dom, n° protein in which those domains exist together
        """

        # {Prot:[list of dmains]}
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        # [(dom, dom)]
        with open("generate_cooccurrence_graph_object_list.json", "r") as jsonFile:
            dom_dom_list = json.load(jsonFile)

        doms_nprot_list=[]
        for dd_tuple in dom_dom_list:
            numbr_prot = 0
            for doms_list in prot_dom_dict.values():
                if dd_tuple[0] in doms_list  and dd_tuple[1] in doms_list :
                    numbr_prot += 1
            dd_tuple = dd_tuple + [numbr_prot,]
            doms_nprot_list.append(dd_tuple)
        return doms_nprot_list


    def generate_cooccurrence_graph_np(self, n):
        """
        This function creates an instance (graph interactions domains)
        where there is interaction between two domains only when they
        cooccurrce in at least n proteins of the graph.

        :param n:int: the threshold of protein selected
        :return: list of tuples: an instance (graph interactions domains)
        """

        doms_t_list=[]
        for dom_dom_npro in self.dom_dom_num_occurence_proteins():
            if dom_dom_npro[2] >= n:
                doms_t_list.append((dom_dom_npro[0], dom_dom_npro[1]))
        self.doms_t_list=list(set(doms_t_list))

        # with open(f"generate_cooccurrence_graph_np{n}.json", "w") as jsonFile:
        #     json.dump(self.doms_t_list, jsonFile)

        return self.doms_t_list

    def graph_n_density(self,n):
        """
        This function returns a graphic that represent the link between
        n (selected number) and the density of graph.

        :param n: int: number of protein selected by the user
        :return: file .png: graph where x: n, y: the density of graph
        """

        n_prot_list=[]
        dens_list=[]
        step=0
        for n_prot in range(0,n):

            step+=1
            print('---',step, '---')

            dom_inter_dict=self.read_interaction_file_dict(self.generate_cooccurrence_graph_np(n_prot))
            number_edges = 0
            for dictio_doms_list in dom_inter_dict.values():
                number_edges += len(dictio_doms_list)
            obj=Interactome('Human_HighQuality.txt')
            dom_list = len(obj.ls_domain())

            #calculate the density
            print('number edges ',number_edges)
            possible_edges = int(int(dom_list) * (int(dom_list) - 1))
            print('edges possible',possible_edges)
            density_graph = number_edges / possible_edges
            dens_list.append(density_graph)
            print('density',density_graph)
            n_prot_list.append(n_prot)

        x=n_prot_list
        y=dens_list
        plt.figure()
        plt.plot(x, y)
        plt.title('Link between number of proteins n and the density \n of the graph of co-occurring domains')
        plt.xlabel('Number of proteins')
        plt.ylabel('Density of domains graph')
        plt.savefig('prot_domgraph.png')
        return plt.show # in our graph in n=62 the number of edges become 0 (no domains in 62 proteins)

    def generate_cooccurrence_graph_n(self, n, redondancy=False):
        """
        This function creat an new instance, whose vertices are the domains.
        There must exist an interaction
        between the vertices x and y only when these two domains are
        co-occurring in at least n times in the proteins of the graph

        :param n: int : redondancy number of domain in the list of domains of a protein
        :param redondancy: False: no interaction between domain and the other domain who has same name
        :return: list of tuples: new instance (domain, domain)
        """

        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)

        dom_dom_tuple_list=[]


        for doms_list in prot_dom_dict.values():
            for dom1 in doms_list:
                shalowcopy = doms_list[:]
                shalowcopy.remove(dom1)
                if doms_list.count(dom1) >= n:
                    for dom2 in shalowcopy:
                        if redondancy ==False:
                            if dom2 != dom1:
                                dom_dom_tuple_list.append((dom1, dom2))
                                dom_dom_tuple_list.append((dom2, dom1))
                        else:
                            dom_dom_tuple_list.append((dom1, dom2))
                            dom_dom_tuple_list.append((dom2, dom1))
        self.domns_cooccur_graph_n=list(set(dom_dom_tuple_list))


        # with open(f"generate_cooccurrence_graph_n{n}.json", "w") as jsonFile:
        #     json.dump(self.domns_cooccur_graph_n, jsonFile)

        return self.domns_cooccur_graph_n


    def creat_file_interaction_dom_dom(self, dom_dom_list, name='dom_dom_interaction.txt', csv_file=False):
        """
        This function export our graph of interactions domain domain in a file

        :param d_d_w: list of tuples: graph:[(dom1,dom2,weight)]
        :param name: name of file by default 'dom_dom_interaction'
        :param: csv file: True for csv file and false for txt file
        :return:file of interaction graph of domains (on txt or on csv)
        """
        if csv_file== False:
            with open(rf'{name}.txt', 'a+', encoding='utf8') as file:
                num_lines = 0
                for tup in dom_dom_list:
                    num_lines += 1
                    file.write('{:<{t}}{:<{t}}{:<{t}}\n'.format(tup[0], tup[1], tup[2], t=20))
            # to add the first line of number of reactions to the file
            with open(rf'{name}.txt', 'r', encoding='utf8') as txt_file:
                file_list = txt_file.readlines()
                file_list.insert(0, f'{num_lines}\n')
            with open(rf'{name}.txt', 'w', encoding='utf8') as txt_file2:
                txt_file2.writelines(file_list)
        else:

            with open(f'{name}.csv', 'w') as file:
                headers = ['Domain_1','Domain_2','Weight']
                line = csv.writer(file)
                line.writerow(headers)

                for tup in dom_dom_list:
                    line.writerow([tup[0], tup[1], tup[2]])

        return 'the file has been created'


    def graph_threshold(self, k):
        """
        This function eliminates from domain graph interactions all edges
        of weight less than k.

        :param k: the threshold selected by the user
        :return: list of tuples: [(dom,dom)]
        """
        # print(len([d_d_weight for d_d_weight in self.weighted_cooccurrence_graph if d_d_weight[2] < k]))
        #Question 6.2.18: number of domains associations that are supported by weight less than 10 : 9718

        return [d_d_weight for d_d_weight in self.weighted_cooccurrence_graph if d_d_weight[2] > k]

    def proba_cooccurrence(self, dom1, dom2):
        """
        Thus function calculate the probability that a protein
        contains exactly two domains contains both domA and domB

        :param dom1: the first domain selected
        :param dom2: the second domain selected
        :return:int: the probability
        """
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        #selection of proteins that have 2 domains
        two_doms_prot_dict={}
        for prot,list_doms in prot_dom_dict.items():
            if len(list_doms)==2:
                two_doms_prot_dict[prot]=list_doms
        #probability= frequency/num total
        obj=Interactome('Human_HighQuality.txt')
        num_cooc=obj.co_occurrence(dom1,dom2,two_doms_prot_dict)
        proba=num_cooc/len(two_doms_prot_dict.values())
        return proba

    #6.3.2 teste de chi2:Comparison of two percentages
    # H0(null hypothesis)= the two frequences are equal,
    # H1 (alternative hypothesis)= the two frequencies are different

    def different_frequency(self):
        """
        This function return pairs of domains whose frequency of co-occurrence
        differs from the theoretical frequency

        :return:list : of domains whose frequency of co-occurrence differs from
        other domains frequencies
        """
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        # selection of proteins that have 2 domains
        two_doms_prot_dict = {}
        for prot, list_doms in prot_dom_dict.items():
            if len(list_doms) == 2:
                two_doms_prot_dict[prot] = list_doms

        frequency_list=[]
        #different from the theorique frequency that mean they have not equel frequency
        #list of tuples [(dom1,dom2),frequency]
        doms_list=[doms for doms in two_doms_prot_dict.values()]
        obj=Interactome('Human_HighQuality.txt')
        for doms_l in doms_list:
            frequency_list.append((doms_l,obj.co_occurrence(doms_l[0],doms_l[1],two_doms_prot_dict)))

        #delete the identical frequencies
        for dom_frq in frequency_list:
            shalow_copy=frequency_list[:]
            for dom_frq2 in shalow_copy:
                if dom_frq[1] == dom_frq2[1]:
                    frequency_list.remove(dom_frq2)
        doms_no_ident_frq=[]
        for doms_f in frequency_list:
            if doms_f[0] not in doms_no_ident_frq:
                doms_no_ident_frq.append(doms_f[0])
        return doms_no_ident_frq


    def proba_cooccurrence_three_doms(self, dom1, dom2):
        """
        Thus function calculate the probability that a protein
        contains exactly three domains contains both domA and domB

        :param dom1: the first domain selected
        :param dom2: the second domain selected
        :return:int: the probability
        """
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        #selection of proteins that have 2 domains
        three_doms_prot_dict={}
        for prot,list_doms in prot_dom_dict.items():
            if len(list_doms)==3:
                three_doms_prot_dict[prot]=list_doms

        #probability= frequency/num total
        obj=Interactome('Human_HighQuality.txt')
        num_cooc=obj.co_occurrence(dom1,dom2,three_doms_prot_dict)
        proba=num_cooc/len(three_doms_prot_dict.values())
        return proba


    def proba_cooccurrence_n_doms(self, dom1, dom2, n):
        """
        Thus function calculate the probability that a protein
        contains exactly n domains contains both domA and domB

        :param dom1: the first domain selected
        :param dom2: the second domain selected
        :param n:number of domains to select the proteins
        :return:int: the probability
        """
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        #selection of proteins that have 2 domains
        n_doms_prot_dict={}
        for prot,list_doms in prot_dom_dict.items():
            if len(list_doms)>=n:
                n_doms_prot_dict[prot]=list_doms

        #probability= frequency/num total
        obj=Interactome('Human_HighQuality.txt')
        num_cooc=obj.co_occurrence(dom1,dom2,n_doms_prot_dict)
        proba=num_cooc/len(n_doms_prot_dict.values())
        return proba


    def different_frequency_n_doms(self,n):
        """
        This function return pairs of domains whose frequency of co-occurrence
        differs from the theoretical frequency for the proteins that have more than n
        domains

        :param n: number of domains wanted to select proteins
        :return:list : of domains whose frequency of co-occurrence differs from
        other domains frequencies
        """
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        # selection of proteins that have more than n domains
        doms_prot_dict = {}
        for prot, list_doms in prot_dom_dict.items():
            if len(list_doms) >= n:
                doms_prot_dict[prot] = list_doms

        frequency_list=[]
        #different from the theorique frequency that mean they have not equel frequency
        #list of tuples [(dom1,dom2),frequency]
        doms_list=[doms for doms in doms_prot_dict.values()]
        obj=Interactome('Human_HighQuality.txt')
        for doms_l in doms_list:
            frequency_list.append((doms_l,obj.co_occurrence(doms_l[0],doms_l[1],doms_prot_dict)))

        #delete the identical frequencies
        for dom_frq in frequency_list:
            shalow_copy=frequency_list[:]
            for dom_frq2 in shalow_copy:
                if dom_frq[1] == dom_frq2[1]:
                    frequency_list.remove(dom_frq2)
        doms_no_ident_frq=[]
        for doms_f in frequency_list:
            if doms_f[0] not in doms_no_ident_frq:
                doms_no_ident_frq.append(doms_f[0])
        return doms_no_ident_frq



