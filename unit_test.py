# -*-coding: UTF-8 -*-
import json
import unittest
from Graph_Interactions import Interactome, Proteome, DomainGraph
import numpy
from os import path

class TestGraphInteractions(unittest.TestCase):

    def test_read_interaction_file_list(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.read_interaction_file_list()
        self.assertEqual(type(result), list)

    def test_read_interaction_file_mat(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.read_interaction_file_mat()
        self.assertEqual(type(result), numpy.ndarray)

    def test_read_interaction_file(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.read_interaction_file()
        self.assertEqual(type(result), tuple)

    def test_count_vertices(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.count_vertices()
        self.assertEqual(type(result), int)

    def test_count_edges(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.count_edges()
        self.assertEqual(type(result), int)

    def test_clean_interactome(self):
        g = Interactome('Human_HighQuality.txt')
        g.clean_interactome()
        result =path.exists('2.1.3_file')
        self.assertEqual(result, True)

    def test_get_degree(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.get_degree('A')
        self.assertEqual(type(result), int)

    def test_proteins_degrees_dict(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.proteins_degrees_dict()
        self.assertEqual(type(result), dict)

    def test_get_max_degree(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.get_max_degree()
        self.assertEqual(type(result), str)

    def test_get_ave_degree(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.get_ave_degree()
        self.assertEqual(type(result), str)

    def test_count_degree(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.count_degree(7)
        self.assertEqual(type(result), str)

    def test_histogram_degree(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.histogram_degree(20,30)
        self.assertEqual(type(result), str)

    def test_component_connex(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.component_connex()
        self.assertEqual(type(result), list)

    def test_countCC(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.countCC()
        self.assertEqual(type(result), str)

    def test_writeCC(self):
        g = Interactome('Human_HighQuality.txt')
        g.writeCC()
        result= path.exists('CC_file')
        self.assertEqual(result, True)

    def test_extractCC(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.extractCC('A')
        self.assertEqual(type(result), list)

    def test_computeCC(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.computeCC()
        self.assertEqual(type(result), list)

    def test_density(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.density()
        self.assertEqual(type(result), float)

    def test_clustering(self):
        g = Interactome('Human_HighQuality.txt')
        result= g.clustering('A')
        self.assertEqual(type(result), float)

    def test_xlinkUniprot(self):
        g=Proteome('uniprot-list.txt')
        result= g.xlinkUniprot()
        self.assertEqual(type(result), dict)

    def test_getProteinDomains(self):
        g=Proteome('uniprot-list.txt')
        result= g.getProteinDomains('1433E_HUMAN')
        self.assertEqual(type(result), list)

    def test_xlinkDomains(self):
        g=Proteome('uniprot-list.txt')
        result= g.xlinkDomains('1433E_HUMAN')
        self.assertEqual(type(result), dict)

    def test_ls_proteins(self):
        obj = Interactome('Human_HighQuality.txt')
        result= obj.ls_proteins()
        self.assertEqual(type(result), list)

    def test_ls_domain(self):
        obj = Interactome('Human_HighQuality.txt')
        result= obj.ls_domain()
        self.assertEqual(type(result), list)

    def test_ls_domains_n(self,n):
        obj = Interactome('Human_HighQuality.txt')
        result= obj.ls_domains_n(n)
        self.assertEqual(type(result), list)

    def test_nbDomainsByProteinDistribution(self):
        obj = Interactome('Human_HighQuality.txt')
        result= obj.nbDomainsByProteinDistribution()
        self.assertEqual(type(result), str)

    def test_nbProteinsByDomainDistribution(self):
        obj = Interactome('Human_HighQuality.txt')
        result= obj.nbProteinsByDomainDistribution()
        self.assertEqual(type(result), str)

    def test_co_occurrence(self):
        obj = Interactome('Human_HighQuality.txt')
        with open("prot_domain_dict.json", "r") as jsonFile:
            prot_dom_dict = json.load(jsonFile)
        result= obj.co_occurrence("RRM_1", "RRM_3", prot_dom_dict)
        self.assertEqual(type(result), int)

    def test_weighted_cooccurrence_graph(self):
        obj = Interactome('Human_HighQuality.txt')
        result= obj.weighted_cooccurrence_graph()
        self.assertEqual(type(result), list)

    def test_generate_cooccurrence_graph(self):
        obj = DomainGraph()
        result= obj.generate_cooccurrence_graph()
        self.assertEqual(type(result), list)

    def test_read_interaction_file_dict(self):
        obj = DomainGraph()
        with open("generate_cooccurrence_graph_object_list.json", "r") as jsonFile:
            dom_dom_list = json.load(jsonFile)
        result= obj.read_interaction_file_dict(dom_dom_list)
        self.assertEqual(type(result), dict)

    def test_density_d(self):
        obj = DomainGraph()
        result= obj.density_d()
        self.assertEqual(type(result), float)

    def test_ten_most_related_dom(self):
        obj = DomainGraph()
        result= obj.ten_most_related_dom()
        self.assertEqual(type(result), list)

    def test_ten_least_related_dom(self):
        obj = DomainGraph()
        result= obj.ten_least_related_dom()
        self.assertEqual(type(result), list)

    def test_most_dom_present_compar_most_related(self):
        obj = DomainGraph()
        result= obj.most_dom_present_compar_most_related()
        self.assertEqual(type(result), bool)

    def test_domains_cooccurring_themselves(self):
        obj = DomainGraph()
        result = obj.domains_cooccurring_themselves()
        self.assertEqual(type(result), int)


    def test_dom_dom_num_occurence_proteins(self):
        obj = DomainGraph()
        result = obj.dom_dom_num_occurence_proteins()
        self.assertEqual(type(result), list)

    def test_generate_cooccurrence_graph_np(self):
        obj = DomainGraph()
        result = obj.generate_cooccurrence_graph_np(6)
        self.assertEqual(type(result), list)

    def test_graph_n_density(self):
        obj = DomainGraph()
        result = obj.graph_n_density(10)
        self.assertEqual(type(result), list)

    def test_generate_cooccurrence_graph_n(self):
        obj = DomainGraph()
        result = obj.generate_cooccurrence_graph_n(5)
        self.assertEqual(type(result), list)

    def test_creat_file_interaction_dom_dom(self):
        obj = DomainGraph()
        with open("generate_cooccurrence_graph_object_list.json", "r") as jsonFile:
            dom_dom_list = json.load(jsonFile)
        result = obj.creat_file_interaction_dom_dom(dom_dom_list)
        self.assertEqual(type(result), str)

    def test_graph_threshold(self):
        obj = DomainGraph()
        result = obj.graph_threshold(5)
        self.assertEqual(type(result), list)

    def test_proba_cooccurrence(self):
        obj = DomainGraph()
        result = obj.proba_cooccurrence("RRM_1", "RRM_3")
        self.assertEqual(type(result), int)

    def test_different_frequency(self):
        obj = DomainGraph()
        result = obj.different_frequency()
        self.assertEqual(type(result), list)

    def test_proba_cooccurrence_three_doms(self):
        obj = DomainGraph()
        result = obj.proba_cooccurrence_three_doms("RRM_1", "RRM_3")
        self.assertEqual(type(result), int)

    def test_proba_cooccurrence_n_doms(self):
        obj = DomainGraph()
        result = obj.proba_cooccurrence_n_doms("RRM_1", "RRM_3", 5)
        self.assertEqual(type(result), int)

    def test_different_frequency_n_doms(self):
        obj = DomainGraph()
        n = 5
        result = obj.different_frequency_n_doms(n)
        self.assertEqual(type(result), int)


if __name__ == '__main__':
    unittest.main()
