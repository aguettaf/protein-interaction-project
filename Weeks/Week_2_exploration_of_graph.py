# -*- coding: utf-8 -*-
"""
@author: Abdelkader Guettaf

"""
from Weeks import Week_1_thibault as w1


# print('******************************************2.1.1*************************************************')

def count_vertices(file):
    """
    this function counts the number of vertices of a graph
    :arg file of proteins interactions
    :return (str) the number of vertices in this graph
    """
    a=w1.read_interaction_file_dict(file)
    n=0
    for i in a.keys():
        n+=1
    return f'the number of vertices of this graph is: {n}'
# print(count_vertices('Human_HighQuality.txt'))
#
# print('******************************************2.1.2*************************************************')

def count_edges(file):
    """
    this function counts the number of edges in a protein interactions graph
    :arg file of proteins interactions
    :return (str) the number of edges in this graph
    """
    n=0
    a=w1.read_interaction_file_dict(file)
    for i in a.values():
        for m in i:
            n+=1
    # return f'the number of edges of this graph is {n}'
    return n


# print(count_edges('Human_HighQuality.txt'))
#
# print('******************************************2.1.3*************************************************')

def clean_interactome(d):
    """
    this function creat a new file without redundant interactions, and without
    the homo-dimers proteins
    :arg d the file of proteins interactions
    :return file : new file
    """
    file=open(d, 'r')
    with file as f:
        file=f.readlines()[1:]
        list_element1=[]
        list_element2=[]
        list_complet=[]
        for line in file:
            l=line.split()
            list_complet.append(l)
            list_element1.append(l[0])
            list_element2.append(l[1])
        element1=list(set(list_element1))
        element2=list(set(list_element2))
        l=[]
        for m in element2:
            for i in element2:
                    if m==i:
                        continue #to do not delete it self
                    elif m[:4]==i[:4]:
                        element2.remove(i)

        # #the difference between homodimers that in their name there are only
        # # the letter with the index 4 that change

        with open(r'2.1.3_file', 'a+',encoding='utf8') as s:
            n=0
            for i in element1:
                for j in element2:
                    for d in list_complet:
                        if [i, j]==d:
                            n+=1
                            s.write(f'{i}	{j}\n')
    #to add the first line of number of reactions to the file
    v=open(r'2.1.3_file', 'r',encoding='utf8')
    l=v.readlines()
    v.close()
    l.insert(0,f'{n}\n')
    v=open(r'2.1.3_file', 'w',encoding='utf8')
    v.writelines(l)
    v.close()
    return 'the file has been created'
# s=clean_interactome('Human_HighQuality.txt')
# print(s)
#
# print('******************************************2.2.1*************************************************')

def get_degree(file, prot):
    """
    this function return the degree (number of relations) of a selected protein
    :arg file the file of proteins interactions
    :arg prot the name of protein wanted
    :return str : the degree of the summet
    """

    dictio=w1.read_interaction_file_dict(file)
    n=0
    for i in dictio.keys():
        if prot == i:
            for num in dictio[i]:
                n+=1
    return(f'the degree of the summet {prot} is {n}')
# print(get_degree('Human_HighQuality.txt', '1433E_HUMAN'))
#
# print('*********************************" I add this function "****************************************')

def proteins_degrees_dict(file):
    """
    this function return a dictionary {protein: his degree}
    the degree (number of relations) of every protein
    :arg file the file of proteins interactions
    :return dict : {protein:degre_of_this_protein}
    """
    dictio=w1.read_interaction_file_dict(file)
    n=0
    d={}# dictionary contains the {prot:number of edges}
    for i,j in dictio.items():
        for num in j:
            n+=1
        d[i] = n
    return d
# print(proteins_degrees_dict('Human_HighQuality.txt'))
#
# print('******************************************2.2.2*************************************************')

def get_max_degree(file):
    """
    this function return the protein that has the maximum number of degrees
    :arg file the file of proteins interactions
    :return (tuple) the protein with max degree, and his number of degree
    """
    d=proteins_degrees_dict(file)
    l = []
    for r in d.values():
        l.append(r)
    l2=[[g,t] for g, t in d.items() if max(l)==t ]
    return f'the maximum number of degrees: protein: {l2[0][0]} number of degree: {l2[0][1]} '
# print(get_max_degree('Human_HighQuality.txt'))
#
# print('******************************************2.2.3*************************************************')

def get_ave_degree(file):
    """
    this function return the average of degrees of all the proteins of this file
    :arg file the file of proteins interactions
    :return int the average of all protein's degrees
    """
    d=proteins_degrees_dict(file)
    sum_value=0
    num_protein=0
    for p in d.values():
        sum_value+=p
        num_protein+=1
    av=sum_value/num_protein
    return (f'The average of degrees: {av}')
# print(get_ave_degree('Human_HighQuality.txt'))
#
# print('******************************************2.2.4*************************************************')

def count_degree(file, deg):
    """
    this function return the number of proteins that have the degree entered by user
    :arg file the file of proteins interactions
    :arg deg the degree wanted
    :return str the name of the protein
    """
    d=proteins_degrees_dict(file)
    num_protein=0
    for i in d.values():
        if i==deg:
            num_protein+=1
    return (f'The number of proteins with the degre of {deg} : {num_protein}')
# print(count_degree('Human_HighQuality.txt', 40))

# print('******************************************2.2.5*************************************************')

def histogram_degree(file, dmin, dmax):
    """
    this function return an histogram with stars, of the number of degrees of each protein
     that exist in the field of degrees entered by user (between dmin, dmax)
    :arg file the file of proteins interactions
    :arg dmin The lower limit of the field wanted
    :arg dmax The higher limit of the field wanted
    :return str histogram with stars
    """

    s=proteins_degrees_dict(file)
    n=0
    pr_hist='The histogram degree: \n'
    for i in s.values():
        if i>=dmin and i<=dmax:
            n+=1
            pr_hist+="\n {:<4}".format(n)
            for m in range(0,i):
                pr_hist+='*'
    return pr_hist
# print(histogram_degree('Human_HighQuality.txt', 40, 60))







