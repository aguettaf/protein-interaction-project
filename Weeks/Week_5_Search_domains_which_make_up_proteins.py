# -*- coding: utf-8 -*-
"""
@author: Abdelkader Guettaf

"""

from Weeks import Week_1_reading_graph_interactions_proteins
import urllib.request

class Proteome ():
    """
    Proteome: is a class which product a dictionary where the keys are proteins and every protein
    has as value another dictionary with three keys : voisins, domains, UniprotID, with lists as values
    new form:
    new_dict={'A':{'domaine':['domaineA1','domaineA2', 'domaineA3', 'domaineA2'], 'voisins':['Prot C', 'Prot B']}}
    """

    def __init__(self, file):
        """
        This constructor creat an object with two attributs (List_prot for liste of proteins
        and dicto for a dioctionary : diction={identifiant UniprotID: protein associated})

        :param file: Uniprot list of identifiants
        """

        li_prot_proteom = []
        dict_id_prot={}

        with open(file, 'r') as uniprot_list:
            next(uniprot_list)
            for line in uniprot_list:
                list_line=line.split()
                li_prot_proteom.append(list_line[0])
                dict_id_prot[list_line[1]]= list_line[0]

        self.list_prot = li_prot_proteom
        self.diction = dict_id_prot #diction{identifiant UniprotID: protein associé}


    def xlinkUniprot(self):
        """
        This function add two keys to the value (dictionnary) of each protein : voisins: for protein neighberhood
        and UniprotID for the identifiant of protein on uniprot

        :return:dict: the new dictionnary :
        new_dict={'A':{'voisins':['Prot C', 'Prot B'], 'UniprotID':'Identifiant_of_protin'}}
        """

        dict_graphe={}
        r = Week_1_reading_graph_interactions_proteins.read_interaction_file_dict("Human_HighQuality.txt")
        for k,v in r.items():
            dictio_inter={}
            dictio_inter['voisins']=v

            for identifiant, prot in self.diction.items():
                if k == prot:
                    dictio_inter['UniprotID'] = identifiant
            dict_graphe[k]=dictio_inter
        return dict_graphe

    def getProteinDomains(self, p):#p protein entré
        """
        This function extract for Uniprot data base the domains of a selected protein.

        :param p:the protein introduced
        :return: list : list of domains of the protein introduced
        """

        p_id=self.xlinkUniprot()[p]['UniprotID']
        with urllib.request.urlopen(f"https://www.uniprot.org/uniprot/{p_id}.txt") as url:
            web_string = str(url.read())  # url.read type is binary
            w_list = web_string.split()
            list_domain = []
            for i in w_list:
                if i == 'Pfam;':
                    for j in w_list[w_list.index(i) + 2::4]:
                        if w_list[w_list.index(j) - 2] == 'Pfam;':
                            list_domain.append(j[:-1])
                        else:
                            break
            list_domain = list(set(list_domain))
            return list_domain

    def xlinkDomains(self, p):
        """
        This function,for each protein introduce, it expand the dictionary data structure
        by adding the protein domains.

        :param p: the introduced protein
        :return:dict : new dictionnary with the domains of the protein selected
        new_dict={'A':{'voisins':['Prot C'], 'UniprotID':'Identifiant','domains':['domain A']}}
        """

        new_dict=self.xlinkUniprot()
        domains=self.getProteinDomains(p)
        new_dict[p]['domains'] = domains
        return new_dict
