# -*-coding: UTF-8 -*-
"""
@author: Guettaf Abdelkader

"""
import numpy as np

# print('******************************************1.2.1*************************************************')


def read_interaction_file_dict(file):
    """
    This function read a protein interaction file and return a dictionary where
    the key is the protein and the value is the related proteins

    :param file: the file of protein interactions (text with tabulation)
    :return: dict {protein:[list of related proteins]}
    """

    l = open(file, 'r')
    next(l)  # add an itiration function to the object that generate one line everytime
    dicto = {}
    for i in l:
        li = i.split()
        if li[0] not in dicto:
            dicto[li[0]] = []
            if li[1] not in dicto[li[0]]:
                dicto[li[0]].append(li[1])
        else:
            if li[1] not in dicto[li[0]]:
                dicto[li[0]].append(li[1])
        if li[1] not in dicto:
            dicto[li[1]] = []
            if li[0] not in dicto[li[1]]:
                dicto[li[1]].append(li[0])
        else:
            if li[0] not in dicto[li[1]]:
                dicto[li[1]].append(li[0])
    l.close()
    return (dicto)


# print(read_interaction_file_dict('Human_HighQuality.txt'))

# print('******************************************1.2.2*************************************************')


def read_interaction_file_list(file):
    """
    this function return from a file of protein interaction, a list of tuples where
    each tuple contains an interaction between two proteins

    :arg: s : the file introduced
    :return: list of tuples
    """

    f = open(file, 'r')
    list = []
    lines_list = f.readlines()[1:]
    for line_list in lines_list:
        element_list = line_list.split()
        i = element_list[0]
        j = element_list[1]
        list.append((i, j))
    f.close()
    return list


# print(read_interaction_file_list('Human_HighQuality.txt'))

# print('******************************************1.2.3*************************************************')


def read_interaction_file_mat(file):
    """
    this function creat an adjacency matrix where in each interaction between
    two proteins it put 1 in the intersection of column and line in the matrix

    :arg: file the file of protein interaction (text with tabs)
    :return: array adjacency matrix of interactions between proteins
    """

    f = open(file, 'r')
    matrix_lines = f.readlines()[1:]
    # i used the set type to delete the dubbled elements in the colums and on the lines
    set_column = []
    set_line = []
    # list_compar contains all the file in alist to use it later in the comparison
    list_compar = []
    for i in matrix_lines:
        list_file = i.split()
        list_compar.append(list_file)
        set_column.append(list_file[0])
        set_line.append(list_file[1])
    line = list(set(set_line))
    column = list(set(set_column))
    # creation of matrix with the dimention of file's line
    matrix = np.zeros((len(line), len(column)))
    for j in column:
        for o in line:
            for m in list_compar:
                if [j, o] == m:
                    matrix[line.index(o)][column.index(j)] = 1

    return matrix


# print(read_interaction_file_mat('Human_HighQuality.txt'))
#
# print('******************************************1.2.4*************************************************')


def read_interaction_file(file):
    """
    this function return a tuple where the first element is the dictionary of file of interactions,
    the second element is a list of the interaction with the first protein of this dictionary

    :arg: file: is the file of interactions (text with tabs)
    :return: tuple ({dictionary, list})
    """

    d_int=read_interaction_file_dict(file)
    l_int=read_interaction_file_list(file)

    return (d_int,l_int)

# print(read_interaction_file('Human_HighQuality.txt'))

