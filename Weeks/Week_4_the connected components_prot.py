from Weeks.Week_1_reading_graph_interactions_proteins import read_interaction_file_dict
from Weeks.Week_2_exploration_of_graph import count_edges

def component_connex(file):
    dictio = read_interaction_file_dict(file)
    list_prot = list(dictio.keys())
    list_complet=[]
    while len(list_prot) !=0:
        list_cc=[list_prot[0]]
        list_prot.remove(list_prot[0])
        for key in list_cc:
            for value in dictio[key]:
                if value not in list_cc:
                    list_cc.append(value)
                    list_prot.remove(value)
        list_complet.append(list_cc)
    return list_complet

# print(component_connex("toy_example.txt"))

def countCC (file):
    cc=component_connex(file)
    pr=''
    for num in cc:
        pr+= f'{len(num)} {num}\n'
    pr+=f'\nle nombre totale est: {len(cc)}'
    return pr
# print(countCC("toy_example.txt"))

def writeCC ():
    with open('new_file','a+') as f:
        f.write(countCC("../Human_HighQuality.txt"))
    return

def extractCC(prot):
    list_com_conx=component_connex("../toy_example.txt")
    for comp_conx in list_com_conx:
        if prot in comp_conx:
            return comp_conx
# print(extractCC('H'))

def computeCC():
    lcc= component_connex("../toy_example.txt")
    n=0
    for cc in lcc:
        n+=1
        for element_cc in cc:
            cc[cc.index(element_cc)]=n
    return lcc
# print(computeCC())

def density():
    number_edges_int=count_edges("toy_example.txt")
    list_prot_int = list(read_interaction_file_dict("toy_example.txt").keys())
    possible_edges_float=len(list_prot_int)*(len(list_prot_int)-1)
    density_graph=number_edges_int/possible_edges_float

    return density_graph

# print(density())

def clustering(prot):
    dicto=read_interaction_file_dict("toy_example.txt")
    num_triangle=0
    pair_neighbour=(len(dicto[prot])*(len(dicto[prot])-1))/2
    for value1 in dicto[prot]:
        for value2 in dicto[value1]:
            if value1 in dicto[value2]:
                num_triangle+=1
    return num_triangle/pair_neighbour

# print(clustering('A'))